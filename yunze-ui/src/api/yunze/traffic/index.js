// 账单
import request from "@/utils/request";

export function postTraffic(data) {
  return request({
    url: '/new/traffic/trafficImportText',
    method: 'post',
    data:data
  })
}
