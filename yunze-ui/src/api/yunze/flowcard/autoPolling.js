import request from '@/utils/request'

// 查询资费组列表
export function autoPollingList(query) {
  return request({
    url: '/yunze/autoPolling/list',
    method: 'post',
    data: query
  })
}

export function autoPollingDel(query) {
  return request({
    url: '/yunze/autoPolling/del',
    method: 'post',
    data: query
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/yunze/autoPolling/importTemplate',
    method: 'get'
  })
}