import request from '@/utils/request'

// 查询生成表数据
export function listUsage(query) {
  return request({
    url: '/usage/cardUsage',
    method: 'post',
    data: query
  })
}
export function cardMonthUsage(query) {
  return request({
    url: '/usage/cardMonthUsage',
    method: 'post',
    data: query
  })
}
