/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:20:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `yz_sys_logs`;
CREATE TABLE `yz_sys_logs`  (
  `id` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `res_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求url',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `class_method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行的类方法',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ip',
  `request_args` longblob NOT NULL COMMENT '请求参数',
  `response_args` longblob NOT NULL COMMENT '响应参数',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作人',
  `project` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块',
  `action_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作名称'
) ENGINE = InnoDB AUTO_INCREMENT = 2009290 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统级日志' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
