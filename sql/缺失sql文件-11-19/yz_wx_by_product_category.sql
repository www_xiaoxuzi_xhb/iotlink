/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:19:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_wx_by_product_category
-- ----------------------------
DROP TABLE IF EXISTS `yz_wx_by_product_category`;
CREATE TABLE `yz_wx_by_product_category`  (
  `category_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `category_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名称',
  `category_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类编码',
  `parent_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父分类ID',
  `category_level` tinyint(4) NOT NULL DEFAULT 1 COMMENT '分类层级',
  `category_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '分类状态',
  `modified_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品分类表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
