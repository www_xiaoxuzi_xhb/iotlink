/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:20:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_wx_by_product_info
-- ----------------------------
DROP TABLE IF EXISTS `yz_wx_by_product_info`;
CREATE TABLE `yz_wx_by_product_info`  (
  `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `product_core` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品编码',
  `product_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品名称',
  `bar_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '国条码',
  `brand_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '品牌表的ID',
  `one_category_id` smallint(5) UNSIGNED NOT NULL COMMENT '一级分类ID',
  `two_category_id` smallint(5) UNSIGNED NULL DEFAULT NULL COMMENT '二级分类ID',
  `three_category_id` smallint(5) UNSIGNED NULL DEFAULT NULL COMMENT '三级分类ID',
  `supplier_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '商品的供应商ID',
  `price` decimal(8, 2) NOT NULL COMMENT '商品销售价格',
  `average_cost` decimal(18, 2) NULL DEFAULT NULL COMMENT '商品加权平均成本',
  `publish_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '上下架状态：0下架1上架',
  `audit_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '审核状态：0未审核，1已审核',
  `weight` float NULL DEFAULT NULL COMMENT '商品重量',
  `length` float NULL DEFAULT NULL COMMENT '商品长度',
  `height` float NULL DEFAULT NULL COMMENT '商品高度',
  `width` float NULL DEFAULT NULL COMMENT '商品宽度',
  `color_type` enum('红','黄','蓝','黑') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `production_date` datetime NULL DEFAULT NULL COMMENT '生产日期',
  `shelf_life` int(11) NULL DEFAULT NULL COMMENT '商品有效期',
  `descript` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '商品描述',
  `indate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '商品录入时间',
  `modified_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品信息表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
