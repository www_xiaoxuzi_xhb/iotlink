/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:18:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_card_usage_reminder
-- ----------------------------
DROP TABLE IF EXISTS `yz_card_usage_reminder`;
CREATE TABLE `yz_card_usage_reminder`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iccid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'iccid',
  `offeringId` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '资费id',
  `offeringName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '资费激活名称',
  `apnName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'APN 名称',
  `totalAmount` double(10, 2) NOT NULL COMMENT '总量，单位：MB',
  `useAmount` double(10, 2) NOT NULL COMMENT '使用量，单位：MB',
  `remainAmount` double(10, 2) NOT NULL COMMENT '剩余量，单位：MB',
  `percentage` double(6, 2) NOT NULL COMMENT '已用百分比',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `upd_date` datetime NOT NULL COMMENT '最近一次修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE,
  INDEX `iccid`(`iccid` ASC, `offeringId` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 638804 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '上游返回套餐记录信息\r\n' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
