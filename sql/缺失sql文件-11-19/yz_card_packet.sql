/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:15:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_card_packet
-- ----------------------------
DROP TABLE IF EXISTS `yz_card_packet`;
CREATE TABLE `yz_card_packet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `package_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '套餐ID',
  `packet_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '套餐包ID',
  `packet_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套餐包名',
  `packet_wx_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信端显示名称',
  `packet_price` double(10, 2) NULL DEFAULT 0.00 COMMENT '套餐包金额',
  `packet_flow` double(10, 2) NULL DEFAULT 0.00 COMMENT '套餐包流量',
  `base_packet_type` int(11) NULL DEFAULT 0 COMMENT '资费计划 类型 0:仅一次，1可叠加',
  `packet_type` int(11) NULL DEFAULT 0 COMMENT '加油包类型：0基础加油包-1叠加油包',
  `is_profit` int(11) NULL DEFAULT 0 COMMENT '是否分润：0否 1是',
  `show_profit` int(11) NULL DEFAULT 0 COMMENT '是否显示分润：0否-1是',
  `wechat_pay` int(11) NULL DEFAULT 1 COMMENT '是否可微信支付：0否1是',
  `balance_pay` int(11) NULL DEFAULT 0 COMMENT '是否可余额支付：0否-1是',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `error_flow` double(10, 2) NULL DEFAULT 0.00 COMMENT '套餐流量',
  `error_so` double(10, 2) NULL DEFAULT 0.00 COMMENT '套餐包SO倍数',
  `packet_valid_time` int(11) NULL DEFAULT NULL COMMENT '资费计划规格\r\n\r\n，根据packet_valid_name定义，表示为月或年',
  `packet_cost` double(10, 2) NULL DEFAULT NULL COMMENT '成本价',
  `is_month` int(11) NULL DEFAULT NULL COMMENT '是否分月到账：0否 1是',
  `date_limit` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '限制不可充值日期',
  `deduction` int(11) NULL DEFAULT NULL COMMENT '是否支持 自动扣代理预存授信延续',
  `in_stock` int(11) NULL DEFAULT NULL COMMENT '是否在售 0 否 1 是',
  `packet_valid_type` int(11) NULL DEFAULT NULL COMMENT '资费计划生效类型 当月生效 yunze_card_takeEffect_type',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '代理编号 与 agent_id 一致 但需要用于系统结构 数据验证',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '系统登录账户id 用于系统结构数据验证',
  `packet_valid_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '月' COMMENT '资费计划规则 月 年 ',
  `card_count` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '卡总数',
  `is_dd` smallint(5) NULL DEFAULT NULL COMMENT '是否订购达量断网',
  `underscorePrice` double(10, 2) NULL DEFAULT 0.00 COMMENT '下划线价格》原价',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `packet_id`(`packet_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 266 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'COLUMNAR=1 资费计划' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
