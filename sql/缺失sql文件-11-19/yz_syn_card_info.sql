/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:19:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_syn_card_info
-- ----------------------------
DROP TABLE IF EXISTS `yz_syn_card_info`;
CREATE TABLE `yz_syn_card_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '接入号',
  `iccid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `imsi` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `activate_date` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '激活时间',
  `open_date` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '开卡时间',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `syn_Time` datetime NULL DEFAULT NULL COMMENT '同步时间',
  `channel_id` int(11) NOT NULL COMMENT '通道编号',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `inconsistent_iccid` smallint(5) NOT NULL DEFAULT 0 COMMENT '是否 与 card_info 表 iccid不一致',
  `is_new` smallint(5) NOT NULL DEFAULT 0 COMMENT '是否 需要新增到 card_info 表',
  `groupId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '上游成员组id',
  `custId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '客户id-[DX_CMP]',
  `status_id` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '卡状态描述 -【DX-CMP】',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `msisdn`(`msisdn` ASC) USING BTREE,
  INDEX `inconsistent_iccid`(`inconsistent_iccid` ASC, `is_new` ASC) USING BTREE,
  INDEX `channel_id`(`channel_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 266456 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '上游返回-群组所属成员信息' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
