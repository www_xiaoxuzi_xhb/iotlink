/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:21:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_card_polling_error
-- ----------------------------
DROP TABLE IF EXISTS `yz_card_polling_error`;
CREATE TABLE `yz_card_polling_error`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iccid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `codeOn` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '错误代码',
  `message` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '错误信息',
  `cd_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通道编码',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `upd_date` datetime NOT NULL COMMENT '最近一次修改时间',
  `is_notice` smallint(6) NOT NULL COMMENT '是否需要通知默认是',
  `rt_map` longblob NOT NULL COMMENT '返回数据',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_iccid`(`iccid` ASC) USING BTREE,
  INDEX `cd_code`(`cd_code` ASC, `iccid` ASC, `codeOn` ASC) USING BTREE,
  INDEX `is_notice`(`is_notice` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '主要用于记录卡用量接口查询时错误记录' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
