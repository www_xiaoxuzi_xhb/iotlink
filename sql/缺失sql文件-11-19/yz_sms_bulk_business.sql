/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:20:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_sms_bulk_business
-- ----------------------------
DROP TABLE IF EXISTS `yz_sms_bulk_business`;
CREATE TABLE `yz_sms_bulk_business`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编号',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `task_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务名',
  `url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '任务下载地址 1,2,3',
  `auth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务创建人',
  `agent_id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '代理编号',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `state_id` smallint(6) NOT NULL COMMENT '状态',
  `message` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '短信内容-主表为单模板批量执行时插入，灵活发送时不插入',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '短信发送-批量任务主表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
