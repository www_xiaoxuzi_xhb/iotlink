/*
 Navicat Premium Data Transfer

 Source Server         : 生产库
 Source Server Type    : MySQL
 Source Server Version : 80013 (8.0.13)
 Source Host           : iotlink.mysql.polardb.cn-chengdu.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013 (8.0.13)
 File Encoding         : 65001

 Date: 19/11/2024 17:19:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_card_api_offeringinfo
-- ----------------------------
DROP TABLE IF EXISTS `yz_card_api_offeringinfo`;
CREATE TABLE `yz_card_api_offeringinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通道类型',
  `offeringId` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '资费 ID',
  `offeringName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '资费名称',
  `apnName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'APN 名称',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `upd_date` datetime NOT NULL COMMENT '最近一次修改时间',
  `type` smallint(6) NOT NULL DEFAULT 1 COMMENT '资费类型',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `offeringId`(`offeringId` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 99 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '上游接口返回已订购资费信息' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
