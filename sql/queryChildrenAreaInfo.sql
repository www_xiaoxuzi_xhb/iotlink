set global log_bin_trust_function_creators=TRUE;

CREATE DEFINER=`root`@`%` FUNCTION `queryChildrenAreaInfo`(
`username_set` char(50) charset utf8mb4 collate utf8mb4_0900_ai_ci ) RETURNS varchar(16383) CHARSET utf8
    SQL SECURITY INVOKER
BEGIN

DECLARE sTemp VARCHAR(16383);
DECLARE sTempChd VARCHAR(16383);

SET sTemp="";
SET sTempChd = CAST(username_set AS CHAR);

WHILE sTempChd IS NOT NULL DO
SET sTemp= CONCAT(sTemp,",",sTempChd);
SELECT GROUP_CONCAT(dept_id) INTO sTempChd FROM sys_dept WHERE FIND_IN_SET(parent_id,sTempChd)>0;
END WHILE;
RETURN sTemp;
END
