package com.yunze.iotapi.service;

import com.yunze.iotapi.utils.ResponseJson;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: Wang
 * @Date: 2024/10/09/15:13
 * @Description:
 */
public interface NewOpenApiService {
    Map<String, Object> queryCardInfoList(HttpServletRequest request);

    Map<String, Object> queryCardDetail(HttpServletRequest request);

    Map<String, Object> queryCardPackage(HttpServletRequest request);


    String mulCardStop(HttpServletRequest request);

    String mulCardStart(HttpServletRequest request);

    ResponseJson mulCardRenewal(HttpServletRequest request);

    Map<String, Object> queryDueExpireDateXQ(HttpServletRequest request);
}
